const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const path = require("path");

module.exports = ({ NODE_ENV = "development" } = {}) => {
  const config = {
    entry: { landing: "./src/landing/landing.js" },
    plugins: [
      new CleanWebpackPlugin(),
      new CopyWebpackPlugin([{ from: "static" }]),
      new HtmlWebpackPlugin({
        favicon: "./static/favicon.ico",
        title: "Hello World",
        template: "src/landing/landing.html"
      })
    ],
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: ["file-loader"]
        }
      ]
    }
  };

  if (NODE_ENV === "production") {
    return {
      mode: "production",
      output: {
        filename: `[name].[hash].js`,
        path: path.resolve(__dirname, "dist")
      },
      ...config
    };
  } else {
    return {
      mode: "development",
      devtool: "inline-source-map",
      output: {
        filename: `[name].bundle.js`,
        path: path.resolve(__dirname, "dist")
      },
      ...config
    };
  }
};
