import "./spin.css";

const spinModule = {
  selectors: {
    target: ".spinable"
  },

  classes: {
    spun: "spinable--spun"
  },

  initialise() {
    window.document
      .querySelectorAll(spinModule.selectors.target)
      .forEach(spinModule.addSpin);
  },

  addSpin(target) {
    const spin = spinModule.spinFactory(target);
    target.onclick = spin;
  },

  spinFactory(el) {
    return () => {
      el.classList.toggle(spinModule.classes.spun);
    };
  }
};

export default spinModule;
