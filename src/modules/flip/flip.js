import "./flip.css";

const flipModule = {
  selectors: {
    container: ".perspective-flipper",
    target: ".flipable"
  },

  classes: {
    button: "button--primary",
    flipped: "flipable--flipped"
  },

  initialise() {
    window.document
      .querySelectorAll(flipModule.selectors.container)
      .forEach(flipModule.setup);
  },

  setup(container) {
    const target = container.querySelector(flipModule.selectors.target);

    if (target === null) {
      console.error(`.flipable not found in container`, container);
      return;
    }

    const button = flipModule.createButton();
    flipModule.addFlip(button, target);
    container.appendChild(button);
  },

  createButton() {
    const button = window.document.createElement("button");

    button.setAttribute("type", "button");
    button.textContent = "Change your perspective";
    button.classList.add(flipModule.classes.button);

    return button;
  },

  addFlip(button, target) {
    const flip = flipModule.flipFactory(target);
    button.onclick = flip;
  },

  flipFactory(el) {
    return () => {
      el.classList.toggle(flipModule.classes.flipped);
    };
  }
};

export default flipModule;
