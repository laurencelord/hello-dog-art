import "./more.css";
import imageUrl from "./more.jpg";

const more = {
  classes: {
    page: "page--more",
    image: "more__image",
    title: "more__title"
  },

  initialise() {
    setTimeout(more.scrollIntoView, 0);

    if (more._initialised) {
      return;
    }
    more._initialised = true;

    window.document.body.appendChild(more.template);
  },

  get template() {
    more._template = more._template || more.generateTemplate();
    return more._template;
  },

  generateTemplate() {
    const page = more.page;
    const title = more.title;
    const image = more.image;
    page.appendChild(image);
    page.appendChild(title);
    return page;
  },

  scrollIntoView() {
    more.template.scrollIntoView({
      behavior: "smooth"
    });
  },

  get page() {
    const page = window.document.createElement("section");
    page.classList.add(more.classes.page);
    return page;
  },

  get title() {
    const title = window.document.createElement("h2");
    title.classList.add(more.classes.title);
    title.textContent = "More";

    return title;
  },

  get image() {
    const image = window.document.createElement("img");
    image.classList.add(more.classes.image);
    image.src = imageUrl;
    return image;
  }
};

export default more;
