import flipModule from "../modules/flip";
import spinModule from "../modules/spin";
import loadModule from "../modules/load";

flipModule.initialise();
spinModule.initialise();
loadModule.initialise();
